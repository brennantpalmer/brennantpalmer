<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Communication $communication
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $communication->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $communication->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Communications'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="communications form large-9 medium-8 columns content">
    <?= $this->Form->create($communication) ?>
    <fieldset>
        <legend><?= __('Edit Communication') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('phone');
            echo $this->Form->control('email');
            echo $this->Form->control('subject');
            echo $this->Form->control('message');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
