<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Example $example
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Example'), ['action' => 'edit', $example->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Example'), ['action' => 'delete', $example->id], ['confirm' => __('Are you sure you want to delete # {0}?', $example->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Examples'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Example'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="examples view large-9 medium-8 columns content">
    <h3><?= h($example->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($example->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($example->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($example->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($example->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($example->modified) ?></td>
        </tr>
    </table>
</div>
