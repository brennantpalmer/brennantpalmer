<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$brennantpalmerdescription = 'Brennan Palmer: IT Professional';
?>
<!DOCTYPE html>
<html>
<head>

    <!-- meta Data -->
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>
        <?= $brennantpalmerdescription ?>
    </title>

    <!-- stylesheet -->
    <link rel="stylesheet" href="css/fontawesome-all.min.css" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/main.css" />
    <link rel="stylesheet" href="css/responsive.css" />
    <link rel="stylesheet" href="css/dark_template.css" />
    <link rel="stylesheet" href="css/colors/default-color.css" />
    <link rel="stylesheet" href="css/custom.css" />

   


    <!-- Plugins -->
    <link rel="stylesheet" href="css/magnific-popup.css" />

    <!-- google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Kaushan+Script|Poppins:300i,400,600" />

    <!-- Font Awesome CDN for the book icon-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
        crossorigin="anonymous">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body>
    <?= $this->Flash->render() ?>
    
    <?= $this->fetch('content') ?>

    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/typed.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
     <!-- For google Recaptcha-->
     <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</body>
</html>
