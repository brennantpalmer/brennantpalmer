
   <!-- Start Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light">

        <div class="container">
            <a href="#" class="logo">
                <span class="bg_logo"></span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <div class="menu">
                    <span class="line1"></span>
                    <span class="line2"></span>
                    <span class="line3"></span>
                </div>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li id="home" class="nav-item active-nav">
                        <a href="#home-cont" data-value="home-cont">
                            <i class="icon-home" hidden></i>
                            <span class="name-menu">Home</span>
                        </a>
                    </li>
                    <li id="about" class="nav-item">
                        <a href="#about-cont" data-value="about-cont">
                            <i class="icon-male-user" hidden></i>
                            <span class="name-menu">About</span>
                        </a>
                    </li>

                    <li id="work" class="nav-item">
                        <a href="#work-cont" data-value="work-cont">
                            <i class="icon-picture" hidden></i>
                            <span class="name-menu">Examples</span>
                        </a>
                    </li>
                    <li id="contact" class="nav-item">
                        <a href="#contact-cont" data-value="contact-cont">
                            <i class="icon-envelope" hidden></i>
                            <span class="name-menu">Contact</span>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="social-media">
                <a href="https://www.linkedin.com/in/brennan-palmer-66b115144/" target="_blank"><i class="fab fa-linkedin"></i></a>                
                <a href="https://bitbucket.org/brennantpalmer/" target="_blank"><i class="fab fa-bitbucket"></i></a>
                <a href="https://github.com/brennantpalmer/" target="_blank"><i class="fab fa-github"></i></a>              
            </div>

            <div class="collapse-menu">
                <span class="collapse-icon closed-menu"></span>
            </div>
        </div>
    </nav>
    <div class="nav-toggle">
        <div class="menu">
            <span class="line1"></span>
            <span class="line2"></span>
            <span class="line3"></span>
        </div>
    </div>
    <!-- End Navbar -->

    <!-- Start Header -->

    <section id="home-cont" class="home1 home">
        <div class="zoominheader bg-image">
            <div class="overlay"></div>
            <div class="zoomoutheader"></div>
        </div>
        <div class="title">
            <h1 class="text-uppercase"><strong>Brennan</strong> Palmer</h1>
            <p class="type"></p> 
        </div>
    </section>

    <!-- End Header -->


    <!-- Start About -->
    <section id="about-cont" class="about">
        <div class="title">
            <h1 class="kaushan">About</h1>
        </div>
        <div class="container">

            <div class="row">

                <div class="col-md-12 col-lg-12">
                    <div class="tabs-about">
                        <ul class="nav nav-tabs text-center" id="myTab" role="tablist">

                            <!-- Start About Me Title Tab -->
                            <li class="nav-item">
                                <a class="nav-link active" id="about-tab" data-toggle="tab" href="#aboutme" role="tab"
                                    aria-controls="about" aria-expanded="true">
                                    <i class="icon-male-user"></i>
                                    <span>About Me</span>
                                </a>
                            </li>
                            <!-- End About Me Title Tab -->


                            <!-- Start Skills Title Tab -->
                            <li class="nav-item">
                                <a class="nav-link" id="skill-tab" data-toggle="tab" href="#skill" role="tab"
                                    aria-controls="skill">
                                    <i class="icon-magic-wand"></i>
                                    <span>Skills</span>
                                </a>
                            </li>
                            <!-- End Skills Title Tab -->

                            <!-- Start Education Title Tab -->
                            <li class="nav-item">
                                <a class="nav-link" id="education-tab" data-toggle="tab" href="#education" role="tab"
                                    aria-controls="education">
                                    <i class="fas fa-book-open"></i>
                                    <span>Education</span>
                                </a>
                            </li>
                            <!-- End Education Title Tab -->
                        </ul>
                        <div class="tab-content" id="myTabContent">

                            <!-- Start About Me Tab -->
                            <div class="tab-pane fade show active text-left" id="aboutme" role="tabpanel"
                                aria-labelledby="about-tab">
                                <?php foreach($abouts as $about): ?>
                                <p>
                                    <?= $about->decription ?>
                                </p>
                                <?php endforeach;?>
                                <img src="images/signature.png" class="img-fluid signature" alt="signature" />
                                <!-- Here image Signature -->
                            </div>
                            <!-- End About Me Tab -->


                            <!-- Start Skills Tab -->
                            <div class="tab-pane fade skill" id="skill" role="tabpanel" aria-labelledby="skill-tab">
                                <div class="skills">

                                    <?php foreach ($skills as $skill): ?>
                               
                                    <p class="progress-text"><?= $skill->title ?></p> 
                                    <div class="progress">
                                        <div class="progress-bar" data-present="<?= $skill->percentage ?>%"></div>
                                        
                                    </div>

                                    <?php endforeach; ?>
                                       
                                </div>                           
                            </div>
                            <!-- End Skills Tab -->

                            <!-- Start Education Tab -->
                            <div class="tab-pane fade text-left" id="education" role="tabpanel" aria-labelledby="education-tab">
                                <?php foreach($educations as $education):?>
                                <p> 
                                    <?= $education->description ?>        
                                </p>
                                <?php endforeach;?>
                            </div>
                            <!-- End Education Tab -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- End About -->


    <!-- Start Work -->
    <section id="work-cont" class="work">
        <div class="title">
            <h1 class="kaushan">Examples</h1>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-12 col-lg-12">
                    <?php foreach($examples as $example):?>
                    <p class="example-title"><?= $example->title ?></p> 
                    <div class="example-separator"></div>
                    <p class="example-description">
                    <?= $example->description ?>
                    </p> 
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </section>

    <!-- End Work -->

    <!-- Start Contact -->
    <section id="contact-cont" class="contact">
        <div class="title">
            <h1 class="kaushan">Contact</h1>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form">
 
                <?= $this->Form->create($communication, ['url' => '/communications/add', 'id' => 'my_form']) ?>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                <?= $this->Form->control('name', ['id' => 'name', 'class' => 'form-control', 'placeholder' => 'Your Name', 'label' => false]) ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <?= $this->Form->control('email', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'Your Email', 'label' => false]) ?>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <?= $this->Form->control('phone', ['id' => 'phone', 'class' => 'form-control', 'placeholder' => 'Your Phone', 'label' => false]) ?>                                    
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                <?= $this->Form->control('subject', ['id' => 'subject', 'class' => 'form-control', 'placeholder' => 'Subject', 'label' => false]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="user-message">
                            <div class="form-group">
                            <?= $this->Form->control('message',  ['id' => 'msg', 'class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Write your message here', 'label' => false]) ?>
                            </div>
                            <p>
                                <span class="send-btn">
                                <?= $this->Form->button('Send Message', ['id' => 'btn', 'class' => 'btn', 'type'> 'submit', 'value' => 'Send Message']) ?>
                                    <i class="fas fa-long-arrow-alt-right"></i>
                                </span>
                                <span id="status"></span>
                            </p>
                        </div>
                        <?= $this->Form->end() ?> 

                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="infos background-gradient">
                    <h2 class="text-uppercase kaushan">BP</h2>
                    <div class="email">
                        <i class="fas fa-envelope"></i>
                        <a href="mailto:brennantpalmer@gmail.com">brennantpalmer@gmail.com</a>
                    </div>
                    
                    <div class="address">
                        <i class="fas fa-map-marker-alt"></i>
                        <span>Tulsa, Oklahoma 74136, United States</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="maps" id="overlay">
            <div class="open-map">
                <span class="o-map active"><i class="fas fa-arrow-alt-circle-down"></i>Open Map</span>
                <span class="c-map"><i class="fas fa-arrow-alt-circle-up"></i>Close Map</span>
            </div>
            <div class="google-wrapper">
                <div id="google-map">                   
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d206181.6942954882!2d-96.01809525101!3d36.152219926445646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x87b692b8ddd12e8f%3A0xe76910c81bd96af7!2sTulsa%2C+OK!5e0!3m2!1sen!2sus!4v1556162772814!5m2!1sen!2sus"></iframe>
                </div>
            </div>
        </div>

        <footer>
            <p>© Copyright 2019. </p>
            <p>All Right Reserved By Brennan Palmer
        </footer>

    </section>

    <!-- End Contact -->


    


