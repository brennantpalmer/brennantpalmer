<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;

/**
 * Communications Controller
 *
 * @property \App\Model\Table\CommunicationsTable $Communications
 *
 * @method \App\Model\Entity\Communication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommunicationsController extends AppController
{
    public function intitialize()
    {
        parent::initialize();
        
    }

    public function beforeFilter(Event $event)
    {
        // allow only add
        $this->Auth->allow('add');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $communications = $this->paginate($this->Communications);

        $this->set(compact('communications'));
    }

    /**
     * View method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $communication = $this->Communications->get($id, [
            'contain' => []
        ]);

        $this->set('communication', $communication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->autoRender = false;
        $communication = $this->Communications->newEntity();
        if ($this->request->is('post')) {

            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                $this->Flash->success(__('The message has been sent.'));

                // Send message to myself
                $email = new Email('default');
                $email->setFrom(['noreply@brennantpalmer.com' => 'brennantpalmer.com'])
                    ->setTo('brennantpalmer@gmail.com')
                    ->setSubject('You have received a message!')
                    ->send($communication->message);


                // Send thank you message back to sender
                $email = new Email('default');
                $email->setFrom(['noreply@brennantpalmer.com' => 'brennantpalmer.com'])
                    ->setTo($communication->email)
                    ->setSubject('I have received your message')
                    ->send(
                        'Thank you for contacting me! You\'re message has been received and I will contact you shortly.
                        
                        Thank you,
                        Brennan Todd Palmer
                        THIS IS AN AUTOMATED MESSAGE.
                        '
                    );

                return $this->redirect(['controller' => 'pages', 'action' => 'display', 'home', '#' => 'contact-cont']);
                //return $this->redirect(['action' => 'index']);

            }

            $this->Flash->error(__('An error has ocurred and the message has not been sent. Please try again.'));
            return $this->redirect(['controller' => 'pages', 'action' => 'display', 'home', '#' => 'contact-cont']);

        }
        $this->set(compact('communication'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $communication = $this->Communications->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $communication = $this->Communications->patchEntity($communication, $this->request->getData());
            if ($this->Communications->save($communication)) {
                $this->Flash->success(__('The communication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The communication could not be saved. Please, try again.'));
        }
        $this->set(compact('communication'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Communication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $communication = $this->Communications->get($id);
        if ($this->Communications->delete($communication)) {
            $this->Flash->success(__('The communication has been deleted.'));
        } else {
            $this->Flash->error(__('The communication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
